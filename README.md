## Ejercicio - Diferencia entre hashes

Analiza los siguientes hashes...

```ruby
key = "Lets Code"
hash = {key: "The Future"}
hash2 = {key => "The Future"}
```

¿Cuál es la diferencia entre los dos hashes?

```

```